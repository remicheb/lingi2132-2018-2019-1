.. _part5:

*************************************************************************************************
Partie 5 | Garbage Collection
*************************************************************************************************

Cheney algorithm proposed by Group 19, Arthur Sluyters and Benjamin Simon
==========================================================================

The Cheney algorithm uses the Forward function. 
Give its pseudo code and explain the cases that it has to handle.

Answer
"""""""
.. code-block:: java

	function Forward(p) {
		if (p points to from-space) {
			if (p.f1 points to to-space) { 
				return p.f1 //already moved: we just need to change the pointer
				
			} else {
				//we have to moved the object from from-space to to-space
				
				for (field fi : p) {
					next.fi = p.fi
				}
				p.f1 = next
				next += size of record p
				return p.f1
			}
		} else {
			return p //the job has already been done: nothing to do
			
		}
	}

Question 2
"""""""""""

Apply the algorithm to the given heap. Explain briefly each step.
	.. image:: img/cheney1.svg
		:alt: Question 2
		:height: 400

Answer
"""""""
	.. image:: img/cheney2.svg
		:alt: Answer2 Part 1
		
	.. image:: img/cheney3.svg
		:alt: Answer2 Part 2
		
		
		
Mark & Sweep Phase proposed by Group 35, Jérémy Kraus and Elias Oumouadene
==========================================================================

1. Clearly explain what are the purpose of the mark and sweep phases
2. Give the pseudo-code of the Sweep phase
3. Explain what is the amortized cost of collection and the complexity of the Sweep phase where H is the heap size

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer :

1. The Mark phase starts from the roots, traces the object in the graph and marks every object reached. 
( A root is an object referenced from anywhere in the call stack )
The Sweep phase iterates through all objects in the heap and reclaims unmarked objects and clear the marks.

2. The pseudo code of the Sweep phase is 
.. code-block:: jave

  p <- first address in heap

  while p < last address in heap

    if record p is marked 
        unmark p
    else let f1 be the first field in p
        p.f1 <- free list

        freelist <- p

    p <- p+(size of record p)

3.The amortized cost of collecting is the time to collect divided by the amount of garbage reclaimed. (c1*R+c2*H)/(H-R)
Sweep = O(H)
